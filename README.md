# Introduction
This is the techical test for UrgentTech

The brief is to create an interface for a legacy application that has a very specific format.  It's intended to demonstrate my skills and knowledge.  The total time spent on this is now 8 hours.

# Getting Started
Download this git repo. I created it using VS community 2017 and it requires .NET core 2.0.  This should be all you need

# Build
To build, just launch VS and run the project.  It should download the required Nuget dependencies (if not just Update-Package from the console should get you going)
You should be able to build the solution without issue.
To test it either run the program from within visual studio, or use a command line.  

```dotnet UrgentTech.dll```

# Test

The project has been set up with the following parameters, so to change the inputters processors or outputters, edit debug -> UrgentTech properties.

```
--processor=JsonProcessor --json-input=files\list.json --output-target=files\output.json --output=console --log=console  --log-to=files\reportlog.log
```

The input file was created using the JsonStronglyTyped processor.  

So if you want to easily create your own tests, either edit this file 
or build a new List<Widget> as shown at the top of program.cs and set --output=file --output-target=files\yourfile.json

# Parameters

```
--processor=[Builder|Json|JsonStronglyTyped]
A processor works its way through a list of widgets and creates a specific type of output
Builder - The format required by the spec
Json - Outputs basic json, no type information is recorded
JsonStronglyTyped - Outputs the json with type info so that a C# App could re-import the file.
```

```
--json-input=PathToFile
The location of the strongly typed JSON file list of widgets to be processed
```

```
--output-target=PathToTarget
For those output types that require a target, such as the File outputter
```

```
--output=[console|file]
Where to push all output to.
```

```
--log=[console]
Where to push all log messages to.  I only implemented console, but log output will appear in red..
```

# Things I didnt do but considered

Due to the time I allocated myself to this I chose not to implement the following.  I know how to, it's just that I could have gone for days just improving the concept.

1. Use a chain design pattern to allow chaining of logs and output formats.
2. Write any sort of --help command into the executable.
3. Write an interface to take user input and fill out the Widget List. Instead input is driven by a serialized json file.
4. Use reflection to take the inputters, processors and outputters from the command line and invoke them instead of the switch statements in the Factory class
5. Move the inputters, processors and outputters into a dll so that (for example) an MVC application could create the list of items to build by reusing the types and calling the methods.
6. More logging. Its always great! but in the timescale I allocated myself, I made sure exceptions and stack traces were logged.

# Known bugs
If a log error occurs, any further console output is in red.  Fixing is as simple as specifying the output colour white in the Console outputter.