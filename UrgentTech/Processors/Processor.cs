﻿using System.Collections.Generic;
using UrgentTech.Interfaces;
using UrgentTech.DebugLoggers;
using UrgentTech.Widgets;
using UrgentTech.OutputRenders;

namespace UrgentTech.Processors
{
    class Processor : ISystemProcessor
    {
        protected List<Widget> Widgets;
        protected DebugLog Log;
        protected OutputRenderer Outputter;

        public Processor(List<Widget> list, OutputRenderer output, DebugLog log)
        {
            Log = log;
            Widgets = list;
            Outputter = output;
        }

        public virtual void Process()
        {
            ValidateItems();
        }

        public virtual bool ValidateItems()
        {
            bool bValid = true;
            foreach (var widget in Widgets)
            {
                bValid = widget.IsValid();

                if (!bValid)
                {
                    break;
                }
            }

            return bValid;
        }
    }
}
