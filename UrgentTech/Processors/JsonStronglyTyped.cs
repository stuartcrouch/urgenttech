﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UrgentTech.DebugLoggers;
using UrgentTech.OutputRenders;
using UrgentTech.Widgets;

namespace UrgentTech.Processors
{
    internal class JsonStronglyTyped : Json
    {
        public JsonStronglyTyped(List<Widget> list, OutputRenderer output, DebugLog log) : base(list, output, log) { }

        public override JsonSerializerSettings Settings => new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            TypeNameHandling = TypeNameHandling.All
        };

    }
}
