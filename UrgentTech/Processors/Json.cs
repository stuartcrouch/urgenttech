﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UrgentTech.DebugLoggers;
using UrgentTech.OutputRenders;
using UrgentTech.Widgets;

namespace UrgentTech.Processors
{
    internal class Json : Processor
    {
        public virtual JsonSerializerSettings Settings => new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore
        };

        public Json(List<Widget> list, OutputRenderer output, DebugLog log) : base(list, output, log) { }

        public override void Process()
        {
            // All processing should begin with validating the objects, base.Process does this
            base.Process();

            try
            {
                string jsonOutput = JsonConvert.SerializeObject(Widgets,
                    Newtonsoft.Json.Formatting.Indented,
                    Settings);

                Outputter.WriteLine(jsonOutput);
            }
            catch (Exception ex)
            {
                Log.WriteLine(ex.ToString() + ex.InnerException + ex.StackTrace);
            }
        }
    }
}
