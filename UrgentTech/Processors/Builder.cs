﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using UrgentTech.DebugLoggers;
using UrgentTech.OutputRenders;
using UrgentTech.Widgets;

namespace UrgentTech.Processors
{
    internal class Builder : Processor
    {
        public Builder(List<Widget> list, OutputRenderer output, DebugLog log) : base(list, output, log) { }

        public override void Process()
        {
            // Builder does not call base validation, as that fills out the validation results allowing processing to continue
            // Instead it validates and uses the response to decide to abort (or not)
            try
            {
                if (!ValidateItems())
                {
                    Outputter.WriteLine("+++++Abort+++++");
                    return;
                }

                Outputter.WriteLine("----------------------------------------------------------------");
                Outputter.WriteLine("Bill of Materials");
                Outputter.WriteLine("----------------------------------------------------------------");

                foreach (var widget in Widgets)
                {
                    Outputter.WriteLine(widget.Render());
                }

                Outputter.WriteLine("----------------------------------------------------------------");
            }
            catch (Exception ex)
            {
                Log.WriteLine(ex.ToString() + ex.InnerException + ex.StackTrace);
            }
        }

    }
}
