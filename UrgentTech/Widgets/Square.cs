﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace UrgentTech.Widgets
{
    class Square : Widget
    {
        public override string WidgetType => "Square";

        [DataMember(Name = "Size")]
        [Range(0, int.MaxValue, ErrorMessage = "Valid range is > 0")]
        public int Size { get; set; }

        public Square(int positionX, int positionY, int size) : base( positionX, positionY )
        {
            Size = size;
        }

        public override string Render()
        {
            return base.Render() +  "size=" + Size.ToString();
        }
    }
}