﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using UrgentTech.Interfaces;

namespace UrgentTech.Widgets
{
    /// <summary>
    /// The base widget class. Contains the values shared by all child types
    /// </summary>
    [DataContract]
    [Serializable]
    class Widget : IBuilderItem
    {
        [DataMember (Name = "Title")]
        public virtual string WidgetType => "WidgetType";

        [DataMember(Name = "PositionX")]
        [Range(0, 1000, ErrorMessage = "Valid range is 0 to 1000.")]
        public int PositionX { get; set; }

        [DataMember(Name = "PositionY")]
        [Range(0, 1000, ErrorMessage = "Valid range is 0 to 1000.")]
        public int PositionY { get; set; }

        public Widget() : this(0,0) { }
        public Widget(int positionX, int positionY)
        {
            PositionX = positionX;
            PositionY = positionY;
        }

        public Widget(SerializationInfo info, StreamingContext context){}

        [DataMember(Name = "Validation Results")]
        public List<ValidationResult> ValidationResults { get; set; }

        /// <summary>
        /// Provides text information to be used for output
        /// </summary>
        /// <returns></returns>
        public virtual string Render()
        {
            return WidgetType + " (" + PositionX.ToString() + "," + PositionY.ToString() + ") ";
        }

        /// <summary>
        /// Uses DataAnnotation to determine if all of the properties 
        /// in the object are valid.
        /// </summary>
        /// <returns>bool</returns>
        public virtual bool IsValid()
        {
            var vc = new ValidationContext(this, null, null);
            ValidationResults = new List<ValidationResult>();
            return Validator.TryValidateObject(this, vc, ValidationResults, true);
        }
        
    }
}
