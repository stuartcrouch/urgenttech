﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace UrgentTech.Widgets
{
    internal class Rectangle : Widget
    {
        public override string WidgetType => "Rectangle";

        [DataMember(Name = "Width")]
        [Range(0, int.MaxValue, ErrorMessage = "Valid range is > 0")]
        public int Width { get; }

        [DataMember(Name = "Height")]
        [Range(0, int.MaxValue, ErrorMessage = "Valid range is > 0")]
        public int Height { get; }

        public Rectangle(int positionX, int positionY, int width, int height) : base(positionX, positionY)
        {
            Width = width;
            Height = height;
        }

        public override string Render()
        {
            return base.Render() + "width=" + Width.ToString() + " height=" + Height.ToString();
        }
    }
}
