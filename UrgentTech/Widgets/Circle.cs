﻿namespace UrgentTech.Widgets
{
    internal class Circle : Square
    {
        public Circle(int positionX, int positionY, int size) : base(positionX, positionY, size)
        {
        }

        public override string WidgetType => "Circle";
    }
}
