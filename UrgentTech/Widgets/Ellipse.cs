﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace UrgentTech.Widgets
{
    internal class Ellipse : Widget
    {
        public override string WidgetType => "Ellipse";

        [DataMember(Name = "Diameter H")]
        [Range(0, int.MaxValue, ErrorMessage = "Valid range is > 0")]
        public int DiameterH { get; }

        [DataMember(Name = "Diameter V")]
        [Range(0, int.MaxValue, ErrorMessage = "Valid range is > 0")]
        public int DiameterV { get; }

        public Ellipse(int positionX, int positionY, int diameterH, int diameterV) : base(positionX, positionY)
        {
            DiameterH = diameterH;
            DiameterV = diameterV;
        }

        public override string Render()
        {
            return base.Render() + "diameterH = " + DiameterH.ToString() + " diameterV = " + DiameterV.ToString();
        }

    }
}
