﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace UrgentTech.Widgets
{
    internal class Textbox : Widget
    {
        public override string WidgetType => "Textbox";

        [DataMember(Name = "Width")]
        [Range(0, int.MaxValue, ErrorMessage = "Valid range is > 0")]
        public int Width { get; }

        [DataMember(Name = "Height")]
        [Range(0, int.MaxValue, ErrorMessage = "Valid range is > 0")]
        public int Height { get; }

        [DataMember(Name = "Text")]
        public string Text { get; }

        public Textbox(int positionX, int positionY, int width, int height, string text) : base(positionX, positionY)
        {
            Width = width;
            Height = height;
            Text = text;
        }

        public override string Render()
        {
            return base.Render() + "width=" + Width.ToString() + " height=" + Height.ToString() + " text=" + Text;
        }
    }
}
