﻿namespace UrgentTech.Interfaces
{
    internal interface ISystemProcessor
    {
        bool ValidateItems();
        void Process();
    }
}
