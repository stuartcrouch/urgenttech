﻿namespace UrgentTech.Interfaces
{
    public interface IBuilderItem
    {
        string Render();
        bool IsValid();
    }
}
