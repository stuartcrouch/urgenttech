﻿namespace UrgentTech.Interfaces
{
    internal interface IOutputRenderer
    {
        void WriteLine(string message);
    }
}
