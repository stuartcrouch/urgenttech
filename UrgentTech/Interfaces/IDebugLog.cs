﻿namespace UrgentTech.Interfaces
{
    internal interface IDebugLog
    {
        void Init();
        void WriteLine(string message);
    }
}
