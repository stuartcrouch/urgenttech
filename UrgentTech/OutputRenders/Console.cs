﻿using System;

namespace UrgentTech.OutputRenders
{
    internal class Console : OutputRenderer
    {

        public override void WriteLine(string message)
        {
            System.Console.WriteLine(message);
        }
    }
}
