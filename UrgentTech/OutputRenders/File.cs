﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace UrgentTech.OutputRenders
{
    class File : OutputRenderer
    {
        public string Filename { get; set; }
        private readonly TextWriter _writer = null;

        public File(string filename)
        {
            Filename = filename;

            Directory.CreateDirectory(Path.GetDirectoryName(Filename));

            _writer = new StreamWriter(Filename);
        }

        ~File()
        {
            _writer?.Close();
        }

        public override void WriteLine(string message)
        {
            _writer?.WriteLine(message);
            _writer?.Flush();
        }
    }
}
