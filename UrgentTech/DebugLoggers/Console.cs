﻿using System;

namespace UrgentTech.DebugLoggers
{
    internal class Console : DebugLog
    {
        public override void Init() { }
        
        public override void WriteLine(string message)
        {
            System.Console.ForegroundColor = ConsoleColor.Red;
            System.Console.WriteLine(message);
        }
    }
}
