﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using UrgentTech.DebugLoggers;
using UrgentTech.OutputRenders;
using UrgentTech.Processors;
using UrgentTech.Widgets;
using Console = System.Console;

namespace UrgentTech
{
    class Program
    {
        static void Main(string[] args)
        {
            //List<Widget> items = new List<Widget>()
            //{
            //    new Rectangle(10, 10, 30, 40),
            //    new Square(15, 30, 35),
            //    new Ellipse(100,150,300,200),
            //    new Circle(1,1,300),
            //    new Textbox(5,5,200,100,"sample text"),

            //   // new Square(-15, 30, 35),
            //};

            try
            {
                // Make the input arguments into a dictionary
                var keyValuePairs = args.Select(value => value.Split('='))
                    .ToDictionary(pair => pair[0].Replace("--", ""), pair => pair[1]);

                DebugLog logger = Factory.DebugLog(keyValuePairs.GetValueOrDefault("log", "")); ;

                // Now we've successfully created a logger, catch any exceptions to there
                try
                {
                    // Load the JSON input file
                    List<Widget> items = null;
                    string jsonContent = System.IO.File.ReadAllText(keyValuePairs.GetValueOrDefault("json-input"));
                    items = JsonConvert.DeserializeObject<List<Widget>>(jsonContent, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });

                    // Load our factory generated objects
                    var outputter = Factory.OutputRenderer(keyValuePairs.GetValueOrDefault("output", ""), keyValuePairs.GetValueOrDefault("output-target"));
                    var processor = Factory.Processor(keyValuePairs.GetValueOrDefault("processor", ""),
                        items, outputter, logger);

                    // Do the work
                    processor?.Process();
                }
                catch (Exception e)
                {
                    logger.WriteLine(e.ToString() + e.InnerException + e.StackTrace);
                }
                
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.ToString() + ex.InnerException + ex.StackTrace);
            }
            
        }
    }
}
