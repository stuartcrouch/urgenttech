﻿using System.Collections.Generic;
using UrgentTech.DebugLoggers;
using UrgentTech.OutputRenders;
using UrgentTech.Processors;
using UrgentTech.Widgets;

namespace UrgentTech
{
    class Factory
    {
        public static Processor Processor(string value,  List<Widget> items, OutputRenderer outputter,
            DebugLog logger)
        {
            Processor processor = null;
            
            switch (value.ToLower())
            {
                case "jsonstronglytyped":
                    processor = new JsonStronglyTyped(items, outputter, logger);
                    break;
                case "json":
                    processor = new Json(items, outputter, logger);
                    break;
                case "xml":

                    break;
                default:
                    processor = new Builder(items, outputter, logger);
                    break;
            }
            return processor;
        }

        public static DebugLog DebugLog(string value)
        {
            DebugLog logger = null;

            switch (value)
            {
                case "file":
                    break;
                default:
                    logger = new DebugLoggers.Console();
                    break;
            }
            return logger;
        }

        public static OutputRenderer OutputRenderer(string value, string outputTarget)
        {
            OutputRenderer outputter = null;

            switch (value)
            {
                case "file":
                    outputter = new File(outputTarget);
                    break;
                default:
                    outputter = new OutputRenders.Console();
                    break;
            }
            return outputter;
        }
    }
}